class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.string :child_fname
      t.string :child_lname
      t.integer :age_group

      t.timestamps
    end
  end
end
