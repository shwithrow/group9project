class CreateInstructors < ActiveRecord::Migration
  def change
    create_table :instructors do |t|
      t.string :instructor_fname
      t.string :instructor_lname
      t.string :instructor_email
      t.string :instructor_phone

      t.timestamps
    end
  end
end
