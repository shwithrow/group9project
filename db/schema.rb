# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131120032529) do

  create_table "children", :force => true do |t|
    t.string   "child_fname"
    t.string   "child_lname"
    t.integer  "age_group"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "classroom_logs", :force => true do |t|
    t.integer  "child_id"
    t.integer  "instructor_id"
    t.integer  "classroom_number"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "course_histories", :force => true do |t|
    t.integer  "instructor_id"
    t.integer  "course_id"
    t.boolean  "course_status"
    t.integer  "child_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "course_history_tables", :force => true do |t|
    t.integer  "instructor_id"
    t.integer  "course_id"
    t.string   "course_status"
    t.integer  "child_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "courses", :force => true do |t|
    t.string   "course_name"
    t.string   "course_description"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "instructors", :force => true do |t|
    t.string   "instructor_fname"
    t.string   "instructor_lname"
    t.string   "instructor_email"
    t.string   "instructor_phone"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

end
