class ClassroomLog < ActiveRecord::Base
  attr_accessible :child_id, :classroom_number, :instructor_id

  belongs_to :child #for child name drop down
  belongs_to :instructor #for instructor name drop down

  validates_existence_of :child
  validates_existence_of :instructor
end
