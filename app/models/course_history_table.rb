class CourseHistoryTable < ActiveRecord::Base
  attr_accessible :child_id, :course_id, :course_status, :instructor_id

  belongs_to :instructor #for instructor name drop down
  belongs_to :course #for course drop down
  belongs_to :child #for age group drop down

  validates_existence_of :instructor
  validates_existence_of :course
  validates_existence_of :child
end
