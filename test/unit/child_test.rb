require 'test_helper'

class ChildTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  setup do
    @child = Child.new
  end

  test "A child must have a first name" do
    #skip "Comment me out to run this test"
    assert_equal @child.fname(), nil, "A child must have a first name to continue."
  end

  test "A child must have a last name" do
    #skip "Comment me out to run this test"
    assert_equal @child.lname(), nil, "A child must have a last name to continue."
  end

  test "A child must have an age group" do
    #skip "Comment me out to run this test"
    assert_equal @child.age(), nil, "A child must have an age group to continue."
  end

end
